
#include "tin/tin.hpp"

CTinParser::CTinParser()
{
	lat = 0;
	lon = 0;
	h = 0;
	course = 0;
	roll = 0;
	pitch = 0;
}

void CTinParser::set(const QRegExp & exp, const QString & str, double & value, const bool is_set)
{
	if(is_set && exp.indexIn(str) > -1)
	{
		QString value_str = exp.cap(1);

		value_str.replace(',', '.');
		value = value_str.toDouble();
	}
}

void CTinParser::operator()(const QString fname)
{
	bool is_lat_set = false, is_lon_set = false;
	QFile fl(fname);
	QTextStream stream;
	QString str;
	QRegExp lat_begin_exp("^\\[LAT_1\\]$"), lat_end_exp("^\\[LON\\]$");
	QRegExp lon_begin_exp("^\\[LON_1\\]$"), lon_end_exp("^\\[Height\\]$");
	QRegExp lat_exp("^[ \\t]*Sh[ \\t]*=[ \\t]*(.*)$");
	QRegExp lon_exp("^[ \\t]*Sh[ \\t]*=[ \\t]*(.*)$");
	QRegExp h_exp("^[ \\t]*H[ \\t]*=[ \\t]*(.*)$");
	QRegExp course_exp("^[ \\t]*Cr[ \\t]*=[ \\t]*(.*)$");
	QRegExp roll_exp("^[ \\t]*Kr[ \\t]*=[ \\t]*(.*)$");
	QRegExp pitch_exp("^[ \\t]*Tan[ \\t]*=[ \\t]*(.*)$");
	QRegExp aspect_x_exp("^[ \\t]*H_CTV[ \\t]*=[ \\t]*(.*)$");
	QRegExp aspect_y_exp("^[ \\t]*V_CTV[ \\t]*=[ \\t]*(.*)$");

	throw_if(! fl.open(QIODevice::ReadOnly | QIODevice::Text));

	stream.setDevice(& fl);

	while(! stream.atEnd())
	{
		str = stream.readLine();

		if(lat_begin_exp.indexIn(str) > -1)
			is_lat_set = true;

		if(lat_end_exp.indexIn(str) > -1)
			is_lat_set = false;

		if(lon_begin_exp.indexIn(str) > -1)
			is_lon_set = true;

		if(lon_end_exp.indexIn(str) > -1)
			is_lon_set = false;

		set(lat_exp, str, lat, is_lat_set);
		set(lon_exp, str, lon, is_lon_set);
		set(h_exp, str, h);
		set(course_exp, str, course);
		set(roll_exp, str, roll);
		set(pitch_exp, str, pitch);

		set(aspect_x_exp, str, aspect_x);
		set(aspect_y_exp, str, aspect_y);
	}

	printf("lat = %lf lon = %lf\n", lat, lon);
	printf("aspect_x = %lf aspect_y = %lf\n", aspect_x, aspect_y);
	printf("h = %lf course = %lf roll = %lf pitch = %lf\n", h, course, roll, pitch);
}

