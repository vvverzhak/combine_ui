
#ifndef TIN_HPP
#define TIN_HPP

#include "all.hpp"

class CTinParser
{
	void set(const QRegExp & exp, const QString & str, double & value, const bool is_set = true);

	public:

		double aspect_x, aspect_y;
		double lat, lon, h, course, roll, pitch;

		CTinParser();

		void operator()(const QString fname);
};

#endif

