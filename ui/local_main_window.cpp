
#include "ui/local_main_window.hpp"

CMainWindow::CMainWindow(const CConfig & config) :
	QMainWindow(NULL), fl(config.local_param_fname())
{
	setupUi(this);

	throw_if(! fl.open(QIODevice::WriteOnly | QIODevice::Text));
	stream.setDevice(& fl);

	map_x_dial->setMinimum(- MAX_XYZ);
	map_y_dial->setMinimum(- MAX_XYZ);
	h_slider->setMinimum(- MAX_XYZ);

	map_x_dial->setMaximum(MAX_XYZ);
	map_y_dial->setMaximum(MAX_XYZ);
	h_slider->setMaximum(MAX_XYZ);

	tool_bar->addAction(QIcon(":/icons/refresh"), trUtf8("О программе"), this, SLOT(reset()));
	tool_bar->addAction(QIcon(":/icons/about"), trUtf8("О программе"), this, SLOT(about()));

	reset();
}

void CMainWindow::about()
{
	CAboutDialog(this).exec();
}

void CMainWindow::set()
{
	fl.reset();
	stream.reset();

	stream << "_pos.x += " << map_x_dial->value_double() / MAX_XYZ << ";" << endl;
	stream << "_pos.y += " << map_y_dial->value_double() / MAX_XYZ << ";" << endl;
	stream << "_pos.z += " << h_slider->value_double() / MAX_XYZ << ";" << endl;
	stream << "_euler_angle.course += " << course_dial->value_double() << ";" << endl;
	stream << "_euler_angle.roll += " << roll_dial->value_double() << ";" << endl;
	stream << "_euler_angle.pitch += " << pitch_dial->value_double() << ";" << endl;
	stream << "_aspect_x = " << aspect_x_dial->value_double() << ";" << endl;
	stream << "_aspect_y = " << aspect_y_dial->value_double() << ";" << endl;

	stream.flush();
	throw_if(! fl.flush());
}

void CMainWindow::reset()
{
	map_x_dial->set_value_double(0);
	map_y_dial->set_value_double(0);
	h_slider->set_value_double(0);
	course_dial->set_value_double(0);
	roll_dial->set_value_double(0);
	pitch_dial->set_value_double(0);
	aspect_x_dial->set_value_double(20);
	aspect_y_dial->set_value_double(20);
}

// ############################################################################ 

#define SET(fun, param, widget)\
void CMainWindow::fun(int value)\
{\
	set();\
}

SET(on_map_x_dial_valueChanged, COMMAND_SET_MAP_X, map_x_dial);
SET(on_map_y_dial_valueChanged, COMMAND_SET_MAP_Y, map_y_dial);
SET(on_h_slider_valueChanged, COMMAND_SET_H, h_slider);
SET(on_aspect_x_dial_valueChanged, COMMAND_SET_ASPECT_X, aspect_x_dial);
SET(on_aspect_y_dial_valueChanged, COMMAND_SET_ASPECT_Y, aspect_y_dial);
SET(on_course_dial_valueChanged, COMMAND_SET_COURSE, course_dial);
SET(on_roll_dial_valueChanged, COMMAND_SET_ROLL, roll_dial);
SET(on_pitch_dial_valueChanged, COMMAND_SET_PITCH, pitch_dial);

