
#ifndef WIDGET_HPP
#define WIDGET_HPP

#include "all.hpp"

class QMyDial : public QDial
{
	Q_OBJECT

	public:

		QMyDial(QWidget * parent);

		virtual double value_double();
		virtual void set_value_double(const double value);
};

class CCoordEdit
{
	const double min_GL = -3, max_GL = 3;
	double a_dial_to_GL, b_dial_to_GL;
	double a_GL_to_dial, b_GL_to_dial;

	public:

		CCoordEdit();

		inline double min() { return 0; };
		inline double max() { return 300; };

		double dial_to_GL(const double value);
		double GL_to_dial(const double value);
};

class QCoordDial : public QMyDial, public CCoordEdit
{
	Q_OBJECT

	public:

		QCoordDial(QWidget * parent);

		double value_double();
		void set_value_double(const double value);
};

class QAngleDial : public QMyDial
{
	Q_OBJECT

	public:

		QAngleDial(QWidget * parent);

		double value_double();
		void set_value_double(const double value);
};

class QAspectDial : public QMyDial
{
	Q_OBJECT

	public:

		QAspectDial(QWidget * parent);

		double value_double();
		void set_value_double(const double value);
};

class QHSlider : public QSlider, public CCoordEdit
{
	Q_OBJECT

	public:

		QHSlider(QWidget * parent);

		double value_double();
		void set_value_double(const double value);
};

#endif

