
#ifndef LOCAL_MAIN_WINDOW_HPP
#define LOCAL_MAIN_WINDOW_HPP

#include "all.hpp"
#include "lib/config.hpp"
#include "ui/about_dialog.hpp"
#include "ui/widget.hpp"
#include "ui_main_window.h"

#define MAX_XYZ 1000000

class CMainWindow : public QMainWindow, private Ui::main_window
{
	Q_OBJECT

	QFile fl;
	QTextStream stream;

	void set();

	public:

		CMainWindow(const CConfig & config);

	public slots:

		void reset();
		void about();

		void on_map_x_dial_valueChanged(int value);
		void on_map_y_dial_valueChanged(int value);
		void on_aspect_x_dial_valueChanged(int value);
		void on_aspect_y_dial_valueChanged(int value);
		void on_h_slider_valueChanged(int value);
		void on_course_dial_valueChanged(int value);
		void on_roll_dial_valueChanged(int value);
		void on_pitch_dial_valueChanged(int value);
};

#endif

