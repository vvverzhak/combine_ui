
#include "ui/widget.hpp"

QMyDial::QMyDial(QWidget * parent) :
	QDial(parent)
{
	;
}

double QMyDial::value_double()
{
	return value();
}

void QMyDial::set_value_double(const double value)
{
	setValue(value);
}

// ############################################################################ 

CCoordEdit::CCoordEdit()
{
	a_dial_to_GL = (max_GL - min_GL) / (max() - min());
	b_dial_to_GL = max_GL - a_dial_to_GL * max();

	a_GL_to_dial = 1. / a_dial_to_GL;
	b_GL_to_dial = max() - a_GL_to_dial * max_GL;
}

double CCoordEdit::dial_to_GL(const double value)
{
	return a_dial_to_GL * value + b_dial_to_GL;
}

double CCoordEdit::GL_to_dial(const double value)
{
	return a_GL_to_dial * value + b_GL_to_dial;
}

// ############################################################################ 

QCoordDial::QCoordDial(QWidget * parent) :
	QMyDial(parent)
{
	setRange(min(), max());
}

double QCoordDial::value_double()
{
	return dial_to_GL(value());
}

void QCoordDial::set_value_double(const double value)
{
	setValue(GL_to_dial(value));
}

// ############################################################################ 

QAngleDial::QAngleDial(QWidget * parent) :
	QMyDial(parent)
{
	const long int max_value = 180 * 3600;

	setRange(- max_value, max_value);
}

double QAngleDial::value_double()
{
	return value() / 3600.;
}

void QAngleDial::set_value_double(const double value)
{
	setValue(value * 3600);
}

// ############################################################################ 

QAspectDial::QAspectDial(QWidget * parent) :
	QMyDial(parent)
{
	setRange(0, 360 * 3600);
}

double QAspectDial::value_double()
{
	return value() / 3600.;
}

void QAspectDial::set_value_double(const double value)
{
	setValue(value * 3600);
}

// ############################################################################ 

QHSlider::QHSlider(QWidget * parent) :
	QSlider(parent)
{
	setRange(min(), max());
}

double QHSlider::value_double()
{
	return dial_to_GL(value());
}

void QHSlider::set_value_double(const double value)
{
	setValue(GL_to_dial(value));
}

