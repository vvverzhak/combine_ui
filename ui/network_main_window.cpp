
#include "ui/network_main_window.hpp"

CMainWindow::CMainWindow(const CConfig & config) :
	QMainWindow(NULL), set_by_one_action(this), command(config)
{
	setupUi(this);

	is_refresh = true;

	tics = 0;
	timer.setInterval(1000);
	timer.start();

	set_by_one_action.setText(trUtf8("Устанавливать по одному"));
	set_by_one_action.setCheckable(true);
	set_by_one_action.setChecked(true);
	QObject::connect(& set_by_one_action, SIGNAL(changed()), this, SLOT(refresh()));

	tool_bar->addAction(& set_by_one_action);
	tool_bar->addAction(QIcon(":/icons/set"), trUtf8("Установить все параметры"), this, SLOT(set_all()));
	tool_bar->addAction(QIcon(":/icons/refresh"), trUtf8("Обновить"), this, SLOT(refresh()));
	tool_bar->addAction(QIcon(":/icons/ping"), trUtf8("Ping"), this, SLOT(ping()));
	tool_bar->addAction(QIcon(":/icons/exit"), trUtf8("Остановить и выйти"), this, SLOT(exit()));
	tool_bar->addAction(QIcon(":/icons/about"), trUtf8("О программе"), this, SLOT(about()));

	QObject::connect(& timer, SIGNAL(timeout()), this, SLOT(refresh_time()));

	try
	{
		refresh();
		refresh_time();
	}
	catch(...)
	{
		QMessageBox::critical(this, trUtf8("Подключение"), trUtf8("Стенд не отвечает"));

		throw_;
	}
}

void CMainWindow::about()
{
	CAboutDialog(this).exec();
}

void CMainWindow::exit()
{
	if(QMessageBox::question(this, trUtf8("Остановить стенд и выйти из консоли?"), trUtf8("Действительно остановить стенд и выйти из консоли?"), QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
	{
		command.exit();
		close();
	}
}

void CMainWindow::ping()
{
	try
	{
		command.ping();

		QMessageBox::information(this, trUtf8("Пинг"), trUtf8("Пинг выполнен успешно"));
	}
	catch(...)
	{
		QMessageBox::critical(this, trUtf8("Пинг"), trUtf8("Стенд не пингуется"));
	}
}

void CMainWindow::refresh()
{
	command.redraw();

	is_refresh = false;

	map_x_dial->set_value_double(command.get(COMMAND_GET_X));
	map_y_dial->set_value_double(command.get(COMMAND_GET_Y));
	aspect_x_dial->set_value_double(command.get(COMMAND_GET_ASPECT_X));
	aspect_y_dial->set_value_double(command.get(COMMAND_GET_ASPECT_Y));
	h_slider->set_value_double(command.get(COMMAND_GET_H));
	course_dial->set_value_double(command.get(COMMAND_GET_COURSE));
	roll_dial->set_value_double(command.get(COMMAND_GET_ROLL));
	pitch_dial->set_value_double(command.get(COMMAND_GET_PITCH)); // TODO * 3600);

	is_refresh = true;
}

void CMainWindow::refresh_time()
{
	bool is_increment = tics % 10;
	
	if(! is_increment)
	{
		try
		{
			SPacketTime time = command.get_time();

			time_edit->setTime(QTime(time.hour, time.min, time.sec));

			tics = 1;
		}
		catch(...)
		{
			is_increment = true;
		}
	}

	if(is_increment)
	{
		// TODO - костыль
		// time_edit->stepUp();
		QTime time = time_edit->time();
		time_edit->setTime(time.addSecs(1));

		tics++;
	}
}

// ############################################################################ 

/*
void CMainWindow::on_x_dial_valueChanged(int value)
{
	command.set_nothrow(COMMAND_SET_X, (2. * value) / MAX_XY - 1);
	
	if(is_refresh)
		refresh();
}

void CMainWindow::on_y_dial_valueChanged(int value)
{
	command.set_nothrow(COMMAND_SET_Y, (2. * value) / MAX_XY - 1);
	
	if(is_refresh)
		refresh();
}
*/

#define SET_AND_REFRESH(fun, param, widget)\
void CMainWindow::fun(int value)\
{\
	if(is_set_by_one())\
	{\
		command.set_nothrow(param, widget->value_double());\
	\
		if(is_refresh)\
			refresh();\
	}\
}

#define SET(fun, param, widget)\
void CMainWindow::fun(int value)\
{\
	if(is_set_by_one())\
		command.set_nothrow(param, widget->value_double());\
}

bool CMainWindow::is_set_by_one()
{
	return set_by_one_action.isChecked();
}

SET_AND_REFRESH(on_map_x_dial_valueChanged, COMMAND_SET_X, map_x_dial);
SET_AND_REFRESH(on_map_y_dial_valueChanged, COMMAND_SET_Y, map_y_dial);
SET(on_h_slider_valueChanged, COMMAND_SET_H, h_slider);
SET(on_aspect_x_dial_valueChanged, COMMAND_SET_ASPECT_X, aspect_x_dial);
SET(on_aspect_y_dial_valueChanged, COMMAND_SET_ASPECT_Y, aspect_y_dial);
SET(on_course_dial_valueChanged, COMMAND_SET_COURSE, course_dial);
SET(on_roll_dial_valueChanged, COMMAND_SET_ROLL, roll_dial);
SET(on_pitch_dial_valueChanged, COMMAND_SET_PITCH, pitch_dial);

void CMainWindow::set_all()
{
	command.set_all_gl_nothrow(map_x_dial->value_double(), map_y_dial->value_double(), h_slider->value_double(), course_dial->value_double(), roll_dial->value_double(), pitch_dial->value_double());
	command.set_nothrow(COMMAND_SET_ASPECT_X, aspect_x_dial->value_double());
	command.set_nothrow(COMMAND_SET_ASPECT_Y, aspect_y_dial->value_double());
	
	refresh();
}

