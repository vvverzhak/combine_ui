
#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include "all.hpp"
#include "lib/command.hpp"
#include "lib/protocol.hpp"
#include "ui/about_dialog.hpp"
#include "ui/widget.hpp"
#include "ui_main_window.h"

class CMainWindow : public QMainWindow, private Ui::main_window
{
	Q_OBJECT

	bool is_refresh;
	unsigned tics;
	QTimer timer;
	QAction set_by_one_action;
	CCommand command;

	bool is_set_by_one();

	public:

		CMainWindow(const CConfig & config);

	public slots:

		void set_all();
		void refresh();
		void refresh_time();
		void ping();
		void exit();
		void about();

//		void on_x_dial_valueChanged(int value);
//		void on_y_dial_valueChanged(int value);
		void on_map_x_dial_valueChanged(int value);
		void on_map_y_dial_valueChanged(int value);
		void on_aspect_x_dial_valueChanged(int value);
		void on_aspect_y_dial_valueChanged(int value);
		void on_h_slider_valueChanged(int value);
		void on_course_dial_valueChanged(int value);
		void on_roll_dial_valueChanged(int value);
		void on_pitch_dial_valueChanged(int value);
};

#endif

