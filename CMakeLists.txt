
cmake_minimum_required(VERSION 2.8)

project(ui)

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Network REQUIRED)
find_package(Qt5OpenGL REQUIRED)
find_package(OpenCV REQUIRED)
find_package(SDL REQUIRED)
find_package(PkgConfig REQUIRED)

pkg_check_modules(PC_AMV QUIET amv)

string(REPLACE ";" " " PC_AMV_CFLAGS " ${PC_AMV_CFLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${PC_AMV_CFLAGS} -Wall -pipe -fPIC -std=c++11" CACHE string "" FORCE)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR} ${Qt5Widgets_INCLUDE_DIRS} ${Qt5Network_INCLUDE_DIRS} ${Qt5OpenGL_INCLUDE_DIRS})

set(combine_udp_sources lib/command.cpp lib/config.cpp)
add_library(combine_udp SHARED ${combine_udp_sources})
target_link_libraries(combine_udp ${PC_AMV_LIBRARIES})

qt5_wrap_ui(dialogs resources/dialog/main_window.ui resources/dialog/about_dialog.ui)
qt5_add_resources(resources resources/resources.qrc)
set(network_gui_sources ${dialogs} ${resources} main_network_gui.cpp ui/network_main_window.cpp ui/about_dialog.cpp ui/widget.cpp)
add_executable(network_gui ${network_gui_sources})
qt5_use_modules(network_gui Core Widgets Network OpenGL)
target_link_libraries(network_gui combine_udp)

set(local_gui_sources ${dialogs} ${resources} main_local_gui.cpp ui/local_main_window.cpp ui/about_dialog.cpp ui/widget.cpp)
add_executable(local_gui ${local_gui_sources})
qt5_use_modules(local_gui Core Widgets Network OpenGL)
target_link_libraries(local_gui combine_udp)

set(video_sources main_video.cpp video/video.cpp)
add_executable(video ${video_sources})
qt5_use_modules(video Core Widgets Network)
target_link_libraries(video combine_udp ${OpenCV_LIBS})

set(joystick_sources main_joystick.cpp)
add_executable(joystick ${joystick_sources})
qt5_use_modules(joystick Core Widgets Network)
target_link_libraries(joystick combine_udp ${SDL_LIBRARY})

set(tin_sources main_tin.cpp tin/tin.cpp)
add_executable(tin ${tin_sources})
qt5_use_modules(tin Core Widgets Network)
target_link_libraries(tin combine_udp ${OpenCV_LIBS})

