
#include "all.hpp"
#include "lib/command.hpp"
#include "tin/tin.hpp"

int main(const int argc, const char * argv[])
{
	throw_if(argc != 3);

	CConfig config(argv[1]);
	CCommand command(config);
	CTinParser parser;
	QDir dir(argv[2]), dir_video(QString(argv[2]) + "/TV_Crop8/");
	QFileInfoList file_list;
	QStringList filters;

	dir_video.setFilter(QDir::Files | QDir::NoSymLinks);

	filters << "*.bmp";
	dir_video.setNameFilters(filters);

	file_list = dir_video.entryInfoList();

	function<bool(const QFileInfo &, const QFileInfo &)> compare_by_name = [] (const QFileInfo & op_1, const QFileInfo & op_2)
	{
		const QString base_name_1 = op_1.baseName(), base_name_2 = op_2.baseName();
		const unsigned length_1 = base_name_1.length(), length_2 = base_name_2.length();

		if(length_1 != length_2)
			return (length_1 < length_2);

		return (base_name_1 < base_name_2);
	};

	qSort(file_list.begin(), file_list.end(), compare_by_name);

	while(1)
		for(auto & file_info : file_list)
		{
			const unsigned ind = file_info.baseName().toUInt();
			Mat img = imread(dir_video.filePath(QString::number(ind) + ".bmp").toStdString());

			parser(dir.filePath(QString::number(ind - 42360) + ".tind"));

//			command.set(COMMAND_SET_ASPECT_X, parser.aspect_x);
//			command.set(COMMAND_SET_ASPECT_Y, parser.aspect_y);
			command.set_all_geo(parser.lat, parser.lon, parser.h, parser.course, parser.roll, parser.pitch);

			imshow("video", img);

			waitKey(40);
		}

	return 0;
}

