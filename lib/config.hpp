
#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "all.hpp"

class CConfig
{
	QString _remote_address, _local_param_fname;
	quint16 _remote_port, _port;

	public:

		CConfig(const QString fname);

		inline QString remote_address() const { return _remote_address; };
		inline quint16 remote_port() const { return _remote_port; };
		inline quint16 port() const { return _port; };
		inline QString local_param_fname() const { return _local_param_fname; };
};

#endif

