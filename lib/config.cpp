
#include "lib/config.hpp"

CConfig::CConfig(const QString fname)
{
	QStringRef name;
	QXmlStreamReader stream;
	QFile fl(fname);

	throw_if(! fl.open(QIODevice::ReadOnly | QIODevice::Text));
	stream.setDevice(& fl);

	while(stream.readNextStartElement())
		while(! stream.atEnd())
		{
			stream.readNext();
			name = stream.name();

			if(name == "remote_address")
				_remote_address = stream.readElementText();
			else if(name == "remote_port")
				_remote_port = stream.readElementText().toUInt();
			else if(name == "port")
				_port = stream.readElementText().toUInt();
			else if(name == "local_param_fname")
				_local_param_fname = stream.readElementText();
		}
}

