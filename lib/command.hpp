
#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "all.hpp"
#include "lib/config.hpp"
#include "lib/protocol.hpp"

class CCommand
{
	const QHostAddress address;
	const quint16 remote_port, port;
	QUdpSocket sock;

	void send_header(const unsigned command, const unsigned size = 0);
	void recv(void * buf, const unsigned buf_size);

	public:

		CCommand(const CConfig & config);

		void exit();
		void ping();
		void redraw();
		double get(const unsigned param);
		SPacketTime get_time();
		void set(const unsigned param, const double value);
		void set_nothrow(const unsigned param, const double value);
		void set_all_gl(const double x, const double y, const double h, const double course, const double roll, const double pitch);
		void set_all_gl_nothrow(const double x, const double y, const double h, const double course, const double roll, const double pitch);
		void set_all_geo(const double lat, const double lon, const double h, const double course, const double roll, const double pitch);
		void set_all_geo_nothrow(const double lat, const double lon, const double h, const double course, const double roll, const double pitch);
		void set_all_gauss_krueger(const double gauss_krueger_x, const double gauss_krueger_y, const double h, const double course, const double roll, const double pitch);
		void set_all_gauss_krueger_nothrow(const double gauss_krueger_x, const double gauss_krueger_y, const double h, const double course, const double roll, const double pitch);
};

#endif

