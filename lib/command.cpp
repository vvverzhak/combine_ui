
#include "lib/command.hpp"

CCommand::CCommand(const CConfig & config) :
	address(config.remote_address()), remote_port(config.remote_port()), port(config.port())
{
	sock.bind(QHostAddress::Any, port);
}

// ############################################################################ 
// Предполагается, что хостовая система - litle-endian

void CCommand::send_header(const unsigned command, const unsigned size)
{
	const SPacketHeader header =
	{
		.command = command,
		.size = size,
		.port = port
	};

	throw_if(sock.writeDatagram((const char *) & header, sizeof(header), address, remote_port) != sizeof(header));
}

void CCommand::recv(void * buf, const unsigned buf_size)
{
	throw_if(! sock.waitForReadyRead(1000));
	sock.readDatagram((char *) buf, buf_size);
}

// ############################################################################ 

void CCommand::exit()
{
	send_header(COMMAND_EXIT);
}

void CCommand::ping()
{
	SPacketPong pong;

	send_header(COMMAND_PING);
	sleep(1);
	recv(& pong, sizeof(pong));

	throw_if(pong.pong != PONG);
}

void CCommand::redraw()
{
	send_header(COMMAND_REDRAW);
}

double CCommand::get(const unsigned param)
{
	SPacketGetSet packet;

	send_header(param);
	recv(& packet, sizeof(packet));

	return packet.value;
}

SPacketTime CCommand::get_time()
{
	SPacketTime time;

	send_header(COMMAND_GET_TIME);
	recv(& time, sizeof(time));

	return time;
}

void CCommand::set(const unsigned param, const double value)
{
	SPacketGetSet packet = { .value = value };

	send_header(param, sizeof(packet));
	throw_if(sock.writeDatagram((const char *) & packet, sizeof(packet), address, remote_port) != sizeof(packet));
}

void CCommand::set_nothrow(const unsigned param, const double value)
{
	try
	{
		set(param, value);
	}
	catch(...)
	{
		;
	}
}

void CCommand::set_all_gl(const double x, const double y, const double h, const double course, const double roll, const double pitch)
{
	SPacketSetAll packet = { .x = x, .y = y, .h = h, .course = course, .roll = roll, .pitch = pitch };

	send_header(COMMAND_SET_ALL_GL, sizeof(packet));
	throw_if(sock.writeDatagram((const char *) & packet, sizeof(packet), address, remote_port) != sizeof(packet));
}

void CCommand::set_all_gl_nothrow(const double x, const double y, const double h, const double course, const double roll, const double pitch)
{
	try
	{
		set_all_gl(x, y, h, course, roll, pitch);
	}
	catch(...)
	{
		;
	}
}

void CCommand::set_all_geo(const double lat, const double lon, const double h, const double course, const double roll, const double pitch)
{
	SPacketSetAll packet = { .x = lat, .y = lon, .h = h, .course = course, .roll = roll, .pitch = pitch };

	send_header(COMMAND_SET_ALL_GEO, sizeof(packet));
	throw_if(sock.writeDatagram((const char *) & packet, sizeof(packet), address, remote_port) != sizeof(packet));
}

void CCommand::set_all_geo_nothrow(const double lat, const double lon, const double h, const double course, const double roll, const double pitch)
{
	try
	{
		set_all_geo(lat, lon, h, course, roll, pitch);
	}
	catch(...)
	{
		;
	}
}

void CCommand::set_all_gauss_krueger(const double gauss_krueger_x, const double gauss_krueger_y, const double h, const double course, const double roll, const double pitch)
{
	SPacketSetAll packet = { .x = gauss_krueger_x, .y = gauss_krueger_y, .h = h, .course = course, .roll = roll, .pitch = pitch };

	send_header(COMMAND_SET_ALL_GAUSS_KRUEGER, sizeof(packet));
	throw_if(sock.writeDatagram((const char *) & packet, sizeof(packet), address, remote_port) != sizeof(packet));
}

void CCommand::set_all_gauss_krueger_nothrow(const double gauss_krueger_x, const double gauss_krueger_y, const double h, const double course, const double roll, const double pitch)
{
	try
	{
		set_all_gauss_krueger(gauss_krueger_x, gauss_krueger_y, h, course, roll, pitch);
	}
	catch(...)
	{
		;
	}
}

