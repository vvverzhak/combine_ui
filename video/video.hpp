
#ifndef VIDEO_HPP
#define VIDEO_HPP

#include "all.hpp"

struct SFrame
{
	Mat frame;
	double x, y, h, course, roll, pitch;
};

class CVideo
{
	FILE * param_fl;
	VideoCapture video;

	public:

		CVideo(const string video_fname, const string param_fname);
		~CVideo();

		bool next_frame(SFrame & frame);
};

#endif
