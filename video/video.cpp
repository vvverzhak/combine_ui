
#include "video/video.hpp"

CVideo::CVideo(const string video_fname, const string param_fname) :
	video(video_fname)
{
	throw_null(param_fl = fopen(param_fname.c_str(), "r"));
}

CVideo::~CVideo()
{
	if(param_fl != NULL)
		fclose(param_fl);
}

bool CVideo::next_frame(SFrame & frame)
{
	double buf[6];

	if(feof(param_fl) || ! video.read(frame.frame))
		return false;

	throw_if(fread(buf, sizeof(double), 6, param_fl) != 6);

	frame.x = buf[0];
	frame.y = buf[1];
	frame.h = buf[2];
	frame.course = buf[3];
	frame.roll = buf[4];
	frame.pitch = buf[5];

	return true;
}

