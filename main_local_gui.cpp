
#include "all.hpp"
#include "lib/config.hpp"
#include "ui/local_main_window.hpp"

int main(int argc, char * argv[])
{
	try
	{
		throw_if(argc < 2);
		CConfig config(argv[1]);

		QApplication app(argc, argv);
		Q_INIT_RESOURCE(resources);

		CMainWindow main_window(config);
		main_window.show();

		return app.exec();
	}
	catch(...)
	{
		;
	}

	return -1;
}

