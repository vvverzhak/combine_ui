
#include "all.hpp"
#include "lib/command.hpp"
#include "video/video.hpp"

int main(const int argc, const char * argv[])
{
	throw_if(argc != 4);

	CConfig config(argv[1]);
	CCommand command(config);
	CVideo video(argv[2], argv[3]);
	SFrame frame;

	command.set(COMMAND_SET_ASPECT_X, 150);
	command.set(COMMAND_SET_ASPECT_Y, 90);

	for(unsigned v = 0; v < 4; v++)
		video.next_frame(frame);

	while(video.next_frame(frame))
	{
		printf("x = %lf y = %lf h = %lf course = %lf roll = %lf pitch = %lf\n", frame.x, frame.y, frame.h, frame.course, frame.roll, frame.pitch);

		imshow("video", frame.frame);

//		while(1)
		{
		command.set_all_gauss_krueger(frame.x, frame.y, frame.h, - frame.course, - frame.roll, frame.pitch);
		waitKey(1000 / 25);
		}
	}

	return 0;
}

