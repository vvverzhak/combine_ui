
#include "all.hpp"
#include "lib/command.hpp"

bool is_roll_update = true, is_pitch_update = true, is_course_update = true;
double x, y, h, roll, pitch, course, vel;
double roll_step, pitch_step, course_step;
double roll_vel_step, pitch_vel_step;
SDL_Joystick * jstk;
CConfig * config;
CCommand * command;

Uint32 timer_callback(Uint32 interval, void * param)
{
	bool is_update = false;

	if(is_course_update && fabs(course_step) > 0.01)
	{
		is_update = true;
		course += course_step;

		if(fabs(course) > 180)
			course *= -1;
	}

	if(is_pitch_update && fabs(pitch_step) > 0.01)
	{
		is_update = true;
		pitch += pitch_step;

		if(fabs(pitch) > 180)
			pitch *= -1;
	}

	if(is_roll_update && fabs(roll_step) > 0.01)
	{
		is_update = true;
		roll += roll_step;

		if(fabs(roll) > 180)
			roll *= -1;
	}

	if(fabs(vel) > 0.9)
	{
		is_update = true;

		const double vel_XY = cos(M_PI * pitch / 180) * vel;
		const double course_right = course - 47;

		x -= sin(M_PI * course_right / 180) * vel_XY;
		y -= cos(M_PI * course_right / 180) * vel_XY;
		h += sin(M_PI * pitch / 180) * vel;
	}

	if(is_update)
		command->set_all_gk_nothrow(x, y, h, course, roll, pitch);

	return interval;
}

Uint32 step_timer_callback(Uint32 interval, void * param)
{
#define VEL_STEP(is_update, vel_step, step) \
	if(is_update && fabs(vel_step) > 0.01)\
		step += vel_step;

	VEL_STEP(is_roll_update, roll_vel_step, roll_step);
	VEL_STEP(is_pitch_update, pitch_vel_step, pitch_step);

	return interval;
}

int main(const int argc, const char * argv[])
{
	throw_if(argc != 2);

	bool is_run = true;
	SDL_Surface * surf;
	SDL_Event event;

	throw_null(config = new CConfig(argv[1]));
	throw_null(command = new CCommand(* config));

	throw_if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_TIMER) < 0);
	throw_null(surf = SDL_SetVideoMode(1, 1, 32, SDL_HWSURFACE | SDL_DOUBLEBUF));
	throw_null(jstk = SDL_JoystickOpen(0));

	x = command->get(COMMAND_GET_GK_X);
	y = command->get(COMMAND_GET_GK_Y);
	h = command->get(COMMAND_GET_H);
	roll = command->get(COMMAND_GET_ROLL);
	pitch = command->get(COMMAND_GET_PITCH);
	course = command->get(COMMAND_GET_COURSE);
	vel = 0;

	SDL_AddTimer(100, & timer_callback, NULL);
	SDL_AddTimer(100, & step_timer_callback, NULL);

	while(is_run)
	{
		SDL_WaitEvent(& event);

		switch(event.type)
		{
			case SDL_KEYDOWN:
			{
				if(event.key.keysym.sym == SDLK_ESCAPE)
					is_run = false;

				break;
			}

#define AXIS_MOTION(axis_ind, is_update, vel_step, step)\
			case axis_ind:\
			{\
				if(is_update)\
				{\
					if(event.jaxis.value < -10000)\
						vel_step = -1;\
					else if(event.jaxis.value > 10000)\
						vel_step = 1;\
					else\
					{\
						vel_step = 0;\
						step = 0;\
					}\
\
				}\
\
				break;\
			}

			case SDL_JOYAXISMOTION:
			{
				switch(event.jaxis.axis)
				{
					AXIS_MOTION(0, is_roll_update, roll_vel_step, roll_step);
					AXIS_MOTION(1, is_pitch_update, pitch_vel_step, pitch_step);
				}

				break;
			}
			case SDL_JOYBUTTONUP:
			{
				switch(event.jbutton.button)
				{
					case 1:
					{
						vel ++;

						printf("Velocity = %lf\n", vel);

						break;
					}
					case 2:
					{
						if(vel > 0)
							vel --;

						printf("Velocity = %lf\n", vel);

						break;
					}
					case 3:
					{
						course_step ++;

						break;
					}
					case 4:
					{
						course_step --;

						break;
					}

#define ON_OFF(button_ind, param_name, is_update)\
					case button_ind:\
					{\
						is_update = ! is_update;\
\
						printf("%s update = %s\n", param_name, is_update ? "ON" : "OFF");\
\
						break;\
					}

					ON_OFF(5, "Roll", is_roll_update);
					ON_OFF(6, "Pitch", is_pitch_update);
				}

				break;
			}
		}
	}

	SDL_JoystickClose(jstk);
	SDL_Quit();

	delete command;
	delete config;

	return 0;
}

