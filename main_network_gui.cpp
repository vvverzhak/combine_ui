
#include "all.hpp"
#include "lib/config.hpp"
#include "ui/network_main_window.hpp"

int main(int argc, char * argv[])
{
	int ret = -1;

	amv_init();

	try
	{
		throw_if(argc < 2);
		CConfig config(argv[1]);

		QApplication app(argc, argv);
		Q_INIT_RESOURCE(resources);

		CMainWindow main_window(config);
		main_window.show();

		ret = app.exec();
	}
	catch(...)
	{
		;
	}

	amv_destroy();

	return ret;
}

