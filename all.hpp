
#ifndef ALL_HPP
#define ALL_HPP

#include <cstdio>
#include <string>
#include <unistd.h>
#include <QDialog>
#include <QMessageBox>
#include <QtCore>
#include <QApplication>
#include <QtNetwork>
#include <QtOpenGL>
#include <QUdpSocket>
#include <QFile>
#include <QXmlStreamReader>
#include <opencv2/opencv.hpp>
#include <SDL/SDL.h>
#include <amv/amv.hpp>

using namespace std;
using namespace cv;

#endif

